#!/usr/bin/env bash

SECONDS=0

DIR=marcxml
if [[ -d $DIR ]]; then
  rm -rf $DIR
fi

ls ids/ids-*.txt | parallel --jobs 6 -I% php get-records.php % $DIR

duration=$SECONDS
hours=$(($duration / (60*60)))
mins=$(($duration % (60*60) / 60))
secs=$(($duration % 60))

printf "%s %s> DONE. %02d:%02d:%02d elapsed.\n" $(date +"%F %T") $hours $mins $secs