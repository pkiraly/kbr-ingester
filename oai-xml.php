<?php
define("LN", "\n");
define('BASE_URL',   'https://opac.kbr.be/oaiserver.ashx?verb=ListIdentifiers');
define('BASE_PARAMS', BASE_URL . '&metadataPrefix=oai_dc&set=QATOOL');
define('FROM_UNTIL',  BASE_PARAMS . '&from=%s&until=%s');
define('TOKEN',       BASE_URL . '&resumptionToken=%s');

require_once 'HTTP/Request2.php';

$ingestType = count($argv) > 1 ? $argv[1] : null;

$lastIngest = null;
$configurationFile = 'configuration.cnf';
if ($ingestType != 'full' && file_exists($configurationFile)) {
  $configuration = parse_ini_file($configurationFile, false, INI_SCANNER_TYPED);
  if (isset($configuration['last-ingest']))
    $lastIngest = $configuration['last-ingest'];
}

$today = date("Y-m-d");
echo "ingestType=$ingestType, lastIngest=$lastIngest, today=$today\n";

$resumptionToken = ''; // '!!QATOOL!3323000!4481990!oai_dc'; // !!QATOOL!3869200!5302096!oai_dc';
$doNext = TRUE;
$time1 = new DateTime("now");

$dir = 'ids';
if (!file_exists($dir))
  mkdir($dir);
else {
  foreach(array_diff(scandir($dir), array('.','..')) as $file)
    unlink($dir . '/' . $file);
}

$i = 0;
$count = 0;
while ($doNext) {
  $doNext = TRUE;
  $xml = getRespone($resumptionToken);
  $ids = [];
  if (isset($xml->ListIdentifiers)) {
    foreach ($xml->ListIdentifiers->header as $header) {
      $rawId = (string) $header->identifier;
      $ids[] = preg_replace('/^KBR/', '', $rawId);
    }
    $count += count($ids);
    file_put_contents(sprintf('%s/ids-%06d.txt', $dir, ++$i), implode(LN, $ids));

    $now2 = new DateTime("now");
    $diffLast = isset($now) ? $now2->diff($now) : $now2->diff($time1);
    $diffTotal = $now2->diff($time1);
    $now = $now2;
    $resumptionToken = $xml->ListIdentifiers->resumptionToken;
    if ($resumptionToken->count() == 0) {
      $doNext = FALSE;
      $percent = 100.0;
    } else {
      $completeListSize = (int) $resumptionToken['completeListSize'];
      $cursor = (int) $resumptionToken['cursor'];
      $percent = $cursor * 100 / $completeListSize;
    }
    $ms = ($diffLast->d * 3600 * 24) + ($diffLast->h * 3600) + ($diffLast->i * 60) + $diffLast->s + $diffLast->f;
    $speed = count($ids) / $ms;
    printf("%s #%d, ingested: %d (%.2f%%), total: %d, speed: %.2f id/s, resumptionToken: %s\n",
      $diffTotal->format('%d %H:%I:%S'), $i, $count, $percent, $completeListSize, $speed, $resumptionToken);
  } else {
    $doNext = FALSE;
  }
}

file_put_contents($configurationFile, "last-ingest=$today");
file_put_contents('statistics.json', json_encode(['ingested-ids' => $count]));

$time2 = new DateTime("now");
$interval=  $time1->diff($time2);
echo 'DONE. It took ', $interval->format('%D %H:%I:%S'), LN;

function getRespone($resumptionToken = '') {
  global $i, $ingestType, $lastIngest, $today;

  if ($resumptionToken == '') {
    if ($ingestType == 'full') {
      $URL = BASE_PARAMS;
    } elseif (!is_null($lastIngest)) {
      $URL = sprintf(FROM_UNTIL, $lastIngest, $today);
    } else {
      $URL = sprintf(FROM_UNTIL, $today, $today);
    }
    echo 'initial URL: ', $URL, LN;
  } else {
    $URL = sprintf(TOKEN, $resumptionToken);
  }

  $trial = 0;
  do {
    $response = getHttpRespone($URL);
    if ($response->status == 200) {
      $xml = simplexml_load_string($response->content) or die("Error: Cannot create object");
    } else {
      printf("%d) trial, i=%d\n", $trial, $i);
      echo 'status: ', $response->status, LN;
      echo 'error message: ', $response->error, LN;
      sleep(3);
      $trial++;
    }
  } while ($trial > 0 && $trial <= 3);

  return $xml;
}

function getHttpRespone($url) {
  $request = new HTTP_Request2($url, HTTP_Request2::METHOD_GET);
  $response = (object)[
    'status' => '',
    'error' => '',
    'content' => '',
  ];

  try {
    $httpResponse = $request->send();
    $response->status = $httpResponse->getStatus();
    if ($httpResponse->getStatus() == 200) {
      $response->content = $httpResponse->getBody();
      // file_put_contents($output, $httpResponse->getBody() . LN, FILE_APPEND);
    } else {
      $response->error = sprintf('Unexpected HTTP status: %s %s', $httpResponse->getStatus(), $httpResponse->getReasonPhrase());
    }
  } catch (HTTP_Request2_Exception $e) {
    $response->error = sprintf('Error: %s', $e->getMessage());
  }

  return $response;
}

