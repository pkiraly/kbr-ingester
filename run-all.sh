#!/usr/bin/env bash

SECONDS=0
INGEST_TYPE=$1

DATE=$(date +"%F")

# ingest all IDs via OAI-PMH
printf "%s %s> 1/5) ingest all IDs via OAI-PMH. Type: %s (logs/oai-pmh.log).\n" $(date +"%F %T") $INGEST_TYPE
php oai-xml.php $INGEST_TYPE > logs/${DATE}.oai-pmh.log

# ingest individual records via z39.50
printf "%s %s> 2/5) ingest individual records via z39.50 (logs/ingest-records.log).\n" $(date +"%F %T")
./ingest-records.sh > logs/${DATE}.ingest-records.log

# grep -cP '^#\d+ ' errors/ids-*.txt | sed 's/^.*://' | paste -d '+' -s | sed 's/+/ + /g' | xargs expr

# import into Mongo
printf "%s %s> 3/5) import into Mongo (logs/mongo-import.log).\n" $(date +"%F %T")
php mongo/import.php marcxml/  > logs/${DATE}.mongo-import.log

# export from Mongo to file system
printf "%s %s> 4/5) export from Mongo to file system (logs/mongo-dump.log).\n" $(date +"%F %T")
php mongo/dump.php > logs/${DATE}.mongo-dump.log

# deploy files
printf "%s %s> 5/5) deploy files (logs/deploy-output.log).\n" $(date +"%F %T")
./deploy-output.sh > logs/${DATE}.deploy-output.log

mv statistics.json ~/logs/${DATE}.statistics.json

MIN=60
HOUR=$(($MIN*60))
DAY=$(($HOUR*24))

duration=$SECONDS
days=$(($duration / $DAY))
duration_=$(($duration % DAY))
hours=$(($duration_ / $HOUR))
duration_=$(($duration_ % $HOUR))
mins=$(($duration_ / $MIN))
duration_=$(($duration_ % $MIN))
secs=$(($duration_))
printf "%s %s> DONE. %d day(s) %02d:%02d:%02d elapsed.\n" $(date +"%F %T") $days $hours $mins $secs
