#!/usr/bin/env bash

SECONDS=0

DUMP=dump
DATA_DIR=~/data

# DATE=2022-02-02
DATE=$(grep -P -o '^last-ingest=\K(.*?)$' configuration.cnf)
# $(date +"%F")
if [[ "$DATE" == "" ]]; then
  DATE=$(date +"%F")
fi

time gzip $DUMP/*.xml
echo 'copy to' $DATA_DIR/$DATE

if [[ ! -d $DATA_DIR/$DATE ]]; then
  mkdir $DATA_DIR/$DATE
else
  rm $DATA_DIR/$DATE/*
fi

cp $DUMP/*.gz $DATA_DIR/$DATE
rm $DATA_DIR/current
ln -s $DATA_DIR/$DATE $DATA_DIR/current

duration=$SECONDS
hours=$(($duration / (60*60)))
mins=$(($duration % (60*60) / 60))
secs=$(($duration % 60))

printf "%s %s> DONE. %02d:%02d:%02d elapsed.\n" $(date +"%F %T") $hours $mins $secs
