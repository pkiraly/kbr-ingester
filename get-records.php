<?php
define("LN", "\n");
define('URL_TEMPLATE', 'https://opac.kbr.be/ils/Z3950QueryHandler.ashx/select?databaseName=EXPLOITATION-BIB-MARC21-nl-BE&area=BIB&syntax=marc21&lang=nl-BE&defType=lucene&q=IDNO:%s');

require_once 'HTTP/Request2.php';

if (count($argv) != 3)
  die('Not enough arguments ' . join($argv, ', '));

$inputFile = $argv[1];
$dir = $argv[2];

printf("download %s into %s\n", $inputFile, $dir);

$outputFile = preg_replace('/^ids\/(.*?)\.txt$/', $dir . '/$1.xml', $inputFile);
$errorFile  = preg_replace('/^ids\/(.*?)\.txt$/', 'errors/$1.txt',  $inputFile);

if (!file_exists('marcxml'))
  mkdir('marcxml');
if (!file_exists('errors'))
  mkdir('errors');
if (file_exists($outputFile))
  unlink($outputFile);
if (file_exists($errorFile))
  unlink($errorFile);

$in = fopen($inputFile, "r");
while (($line = fgets($in)) != false) {
  download(trim($line));
}
fclose($in);

function download($id) {
  global $outputFile, $errorFile;

  $url = sprintf(URL_TEMPLATE, $id);
  // echo $url, LN;

  $request = new HTTP_Request2($url, HTTP_Request2::METHOD_GET);
  $error = '';
  try {
    $response = $request->send();
    if (200 == $response->getStatus()) {
      $content = $response->getBody();
      if (preg_match('/<record/', $content) && preg_match('/(<\/record>)/', $content)) {
        $content = preg_replace('/^.*?(<record)/', '$1', $content);
        $content = preg_replace('/(<\/record>).*?$/', '$1', $content);
        file_put_contents($outputFile, $content . LN, FILE_APPEND);
      } else {
        $error = sprintf('strange content: %s', $content);
      }
    } else {
      $error = sprintf('Unexpected HTTP status: %s %s', $response->getStatus(), $response->getReasonPhrase());
    }
  } catch (HTTP_Request2_Exception $e) {
    $error = sprintf('Error: %s', $e->getMessage());
  }

  if ($error != '')
    file_put_contents($errorFile, sprintf("#%d %s", $id, $error) . LN, FILE_APPEND);
}
