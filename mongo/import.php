<?php
// connect to mongodb
define('LN', "\n");

require_once __DIR__ . '/../vendor/autoload.php';

$collection = (new MongoDB\Client)->marc->record;

$dir = $argv[1];
if (!file_exists($dir))
  exit("The input directory $dir does not exist\n");
if (!preg_match('/\/$/', $dir))
  $dir .= '/';

$onlyInsert = (isset($argv[2]) && $argv[2] == 'onlyInsert');

$statFile = __DIR__ . '/../statistics.json';
printf("statfile: %s, exist? %d\n", realpath($statFile), (int) file_exists($statFile));
$statistics = json_decode(file_get_contents($statFile));
$statistics->inserted = 0;
$statistics->modified = 0;
$statistics->identical = 0;
$time1 = new DateTime("now");
// $dir = '../marcxml/';
$indir = scandir($dir);
foreach ($indir as $file) {
  if (preg_match('/\.xml$/', $file)) {
    // if ($file == 'kbr-4.xml') {
      echo $dir . $file, LN;
      importFileToMongo($dir . $file, $onlyInsert);
    // }
  }
}


// $statistics->records = $collection->countDocuments();
$statistics->records = getCount();
echo 'statistics: ', json_encode($statistics), LN;
file_put_contents($statFile, json_encode($statistics));

$time2 = new DateTime("now");
$interval=  $time1->diff($time2);
echo 'DONE. It took ', $interval->format('%H:%I:%S'), LN;

function importFileToMongo($file, $onlyInsert) {
  global $collection, $statistics;

  $handle = fopen($file, 'r');
  if ($handle) {
    while (($line = fgets($handle)) !== false) {
      if (preg_match('/<\?xml|<\/?collection>/', $line)) {
        continue;
      }

      if (preg_match('/<record /', $line) && preg_match('/<\/record>/', $line)) {
        $content = $line;
      } else if (preg_match('/<record /', $line)) {
        $content = $line;
      } else {
        $content .= $line;
      }

      if (preg_match('/<record /', $content) && preg_match('/<\/record>/', $content)) {
        if (preg_match('/<controlfield tag="001">(.+?)<\/controlfield>/', $content, $matches)) {
          $id = $matches[1];
          if ($onlyInsert || is_null($collection->findOne(['id' => $id]))) {
            $insertOneResult = $collection->insertOne([
             'id' => $id,
             'record' => $content,
            ]);
            if (!$onlyInsert)
              printf("Inserted %d document(s)\n", $insertOneResult->getInsertedCount());
            $statistics->inserted += $insertOneResult->getInsertedCount();
          } else {
            $updateResult = $collection->updateOne(
             ['id' => $id],
             ['$set' => ['record' => $content]]
            );
            // printf("Matched %d document(s)\n", $updateResult->getMatchedCount());
            // printf("Updated %d document(s)\n", $updateResult->getModifiedCount());
            if ($updateResult->getModifiedCount() > 0) {
              $statistics->modified += $updateResult->getModifiedCount();
            } else {
              $statistics->identical += $updateResult->getMatchedCount();
            }
          }
        } else {
          echo 'error: no ID found. ', $content, LN;
        }
      }
    } // while
    fclose($handle);
  } else {
    echo 'error opening the file.', LN;
  }
}

function getCOunt() {
  $output = null;
  $retval = null;
  exec("mongosh marc --eval 'db.record.countDocuments({})'", $output, $retval);
  if ($retval == 0) {
    return end($output);
  }
  return FALSE;
}
