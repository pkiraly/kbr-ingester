<?php
// connect to mongodb
define('LN', "\n");
define('HEADER', '<?xml version="1.0" encoding="UTF-8" ?>' . LN . '<collection>' . LN);
define('FOOTER', '</collection>' . LN);

require_once __DIR__ . '/../vendor/autoload.php';

$collection = (new MongoDB\Client)->marc->record;

$cursor = $collection->find([],[]);

$dir = 'dump';
if (!file_exists($dir))
  mkdir($dir);

foreach(array_diff(scandir($dir), array('.','..')) as $file)
  unlink($dir . '/' . $file);

$time1 = new DateTime("now");
$i = 0;
$j = 0;
$filename = getFilename($j);
file_put_contents($filename, HEADER);
foreach ($cursor as $document) {
  $i++;
  // printf("%s: %s\n", $document['id'], $document['record']);
  file_put_contents($filename, $document['record'], FILE_APPEND);

  if ($i % 10000 == 0)
    echo $i, ' ', $filename, LN;

  if ($i % 1000000 == 0) {
    file_put_contents($filename, FOOTER, FILE_APPEND);
    $filename = getFilename(++$j);
    file_put_contents($filename, HEADER);
  }
}
file_put_contents($filename, FOOTER, FILE_APPEND);

$time2 = new DateTime("now");
$interval=  $time1->diff($time2);
echo 'DONE. It took ', $interval->format('%H:%I:%S'), LN;

function getFilename($j) {
  global $dir;

  return $dir . '/kbr-' . $j . '.xml';
}
