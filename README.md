# KBR ingester

Ingest catalogue records from KBR 


## Prerequisites


```bash
# for ingestion
sudo apt install php php-dev php-pear
sudo pear install HTTP_Request2

# for parallel ingestion of records
sudo apt install parallel

# for import to and export from Mongo

# install the low level MongoDB driver
sudo apt install mongodb

# install the high level MongoDB PHP library
sudo pecl install mongodb
sudo nano /etc/php/7.4/apache2/php.ini # add extension=mongodb.so
sudo nano /etc/php/7.4/cli/php.ini     # add extension=mongodb.so

# install composer (follow instructions from https://getcomposer.org/download/)
php -r "copy('https://getcomposer.org/installer', 'composer-setup.php');"
php -r "if (hash_file('sha384', 'composer-setup.php') === '906a84df04cea2aa72f40b5f787e49f22d4c2f19492ac310e8cba5b96ac8b64115ac402c8cd292b8a03482574915d1a8') { echo 'Installer verified'; } else { echo 'Installer corrupt'; unlink('composer-setup.php'); } echo PHP_EOL;"
php composer-setup.php
php -r "unlink('composer-setup.php');"
sudo mv composer.phar /usr/local/bin/composer

composer require mongodb/mongodb
```

## Usage

```bash
# ingest all IDs via OAI-PMH
nohup php oai-xml.php > logs/oai-pmh.log &

# ingest individual records via z39.50
nohup ./ingest-records.sh > logs/ingest-records.log &

# import into Mongo
nohup php mongo/import.php marcxml/  > logs/mongo-import.log &

# export from Mongo to file system
nohup php mongo/dump.php > logs/mongo-dump.log &

# compress files
nohup ./deploy-output.sh > logs/deploy-output.log &
```
